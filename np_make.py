import os
import sys
import cv2
import numpy as np

from CLI.utils import breaker


def reserve_memory(path: str, size: int, kaggle: bool) -> tuple:
    total_num_files: int = 0
    for name in os.listdir(path):
        if "." in name or name == "test":
            pass
        else:
            total_num_files += len(os.listdir(os.path.join(path, name)))
    if kaggle:
        total_num_files -= 2
    images = np.zeros((total_num_files, size, size, 3), dtype=np.uint8)
    labels = np.ones((total_num_files, 1), dtype=np.uint8)
    return images, labels


def main() -> None:
    args_1: tuple = ("--path", "-p")
    args_2: tuple = ("--size", "-s")
    args_3: tuple = ("--kaggle", "-k")

    path: str = "data"
    size: int = 320
    kaggle: bool = False

    if args_1[0] in sys.argv: path = sys.argv[sys.argv.index(args_1[0]) + 1]
    if args_1[1] in sys.argv: path = sys.argv[sys.argv.index(args_1[1]) + 1]

    if args_2[0] in sys.argv: size = int(sys.argv[sys.argv.index(args_2[0]) + 1])
    if args_2[1] in sys.argv: size = int(sys.argv[sys.argv.index(args_2[1]) + 1])

    if args_3[0] in sys.argv or args_3[1] in sys.argv: kaggle = True

    assert os.path.exists(path), "Please Unzip the data first using python unzip.py"

    breaker()
    print("Reserving Memory ...")

    images, labels = reserve_memory(path, size, kaggle)

    print(images.shape)

    i: int = 0
    j: int = 0

    breaker()
    print("Saving Images to Numpy Arrays ...")

    folders = sorted([folder_name for folder_name in os.listdir(path) if "." not in folder_name and folder_name != "test"])

    for folder_name in folders:
        for filename in os.listdir(os.path.join(path, folder_name)):
            if kaggle:
                if filename == "4514.jpg" or filename == "1187.jpg":
                    i -= 1
                else:
                    images[i] = cv2.resize(src=cv2.cvtColor(src=cv2.imread(os.path.join(os.path.join(path, folder_name), filename), cv2.IMREAD_COLOR), code=cv2.COLOR_BGR2RGB), dsize=(size, size), interpolation=cv2.INTER_AREA)
                    labels[i] = labels[i] * j
            else:
                images[i] = cv2.resize(src=cv2.cvtColor(src=cv2.imread(os.path.join(os.path.join(path, folder_name), filename), cv2.IMREAD_COLOR), code=cv2.COLOR_BGR2RGB), dsize=(size, size), interpolation=cv2.INTER_AREA)
                labels[i] = labels[i] * j
            i += 1
        j += 1

    if kaggle:
        np.save("./images.npy", images)
        np.save("./labels.npy", labels)
    else:
        np.save(os.path.join(path, "images.npy"), images)
        np.save(os,path.join(path, "labels.npy"), labels)

    breaker()
    print("Saving Complete")
    breaker()


if __name__ == "__main__":
    sys.exit(main() or 0)